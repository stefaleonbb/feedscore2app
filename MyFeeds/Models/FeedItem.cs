﻿using System;

namespace MyFeeds.Models
{
    public class FeedItem
    {
        public int Id { get; set; }

        public Feed Feed { get; set; }
        public int FeedId { get; set; }

        public string Link { get; set; } // CodeHollow item.Link

        public DateTime? PublishingDate { get; set; }  // CodeHollow item.PublishingDate

        public string PublishingDateString { get; set; }  // CodeHollow item.PublishingDateString

        public string Title { get; set; } // CodeHollow item.Title
    }
}