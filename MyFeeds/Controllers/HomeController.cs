﻿using CodeHollow.FeedReader;
using Microsoft.AspNetCore.Mvc;
using MyFeeds.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace MyFeeds.Controllers
{
    public class HomeController : Controller
    {
        private FeedRepository repo;

        public HomeController(FeedRepository feedRepo)
        {
            repo = feedRepo;
        }



        public ViewResult Index() => View(repo.Feeds.OrderBy(f => f.Title));



        public IActionResult Subscribe()
        {
            return View();
        }



        [HttpPost]
        public IActionResult AddNewFeed(Models.Feed newFeed)
        {
            if (string.IsNullOrEmpty(newFeed.Url))
                return RedirectToAction("InvalidFeedLink");

            try
            {
                // use CodeHollow.FeedReader to get the Title                
                newFeed.Title = FeedReader.ReadAsync(newFeed.Url).Result.Title;
            }
            catch (Exception)
            {
                return RedirectToAction("InvalidFeedLink");
            }
            
            newFeed.OwnerId = 42; // until auth is configured            

            if (ModelState.IsValid)
            {
                // if not already in db, save to db                    
                var feedExists = repo.Feeds.Any(f => f.Title == newFeed.Title);
                if (!feedExists)
                {                    
                    repo.SaveFeed(newFeed);

                    // get the new record's Id
                    int newId = repo.Feeds.SingleOrDefault(f => f.Title == newFeed.Title).Id;
                    // read and save feeditems                    
                    var newItems = new List<Models.FeedItem>();
                    ReadItems(newFeed, newId, newItems);
                    SaveNewItems(newFeed, newItems);
                }

                return RedirectToAction("Index");
            }

            return View();
        }



        public void ReadItems(Models.Feed currentFeed, int currentFeedId, List<Models.FeedItem> currentItems)
        {
            // use CodeHollow.FeedReader to get the CH-Items-props and map them to the Model-items-props
            var feedReader = FeedReader.ReadAsync(currentFeed.Url);
            var items = feedReader.Result.Items;

            foreach (var item in items)
            {
                var newItem = new Models.FeedItem
                {
                    FeedId = currentFeedId,
                    Link = item.Link,
                    PublishingDate = item.PublishingDate,
                    PublishingDateString = item.PublishingDateString,
                    Title = item.Title
                };
                currentItems.Add(newItem);
            }
        }



        public void SaveNewItems(Models.Feed currentFeed, List<Models.FeedItem> currentItems)
        {
            foreach (var item in currentItems)
            {
                // if not already in db, save to db                    
                var linkExists = repo.FeedItems.Any(i => i.Link == item.Link);
                if (!linkExists && ModelState.IsValid)
                {
                    repo.SaveFeedItem(item);
                }
            }
        }



        [HttpPost]
        public IActionResult DisplayFeedItems(int feedId)
        {
            var selectedFeed = repo.Feeds.FirstOrDefault(f => f.Id == feedId);
            var currentItems = new List<Models.FeedItem>();

            try
            {
                ReadItems(selectedFeed, feedId, currentItems);
            }
            catch (Exception)
            {
                return RedirectToAction("InvalidFeedLink");
            }

            // save newlly seen items
            SaveNewItems(selectedFeed, currentItems);

            if (selectedFeed != null)
            {
                return View(new FeedViewModel
                {
                    Feed = selectedFeed,
                    FeedItems = currentItems
                });
            }
            //return Ok("Empty Feed object!");  // todo: a view...
            return RedirectToAction("NoNews");
        }



        public IActionResult InvalidFeedLink()
        {
            return View();
        }



        [HttpPost]
        public IActionResult Delete(int Id)
        {
            repo.DeleteFeed(Id);            
            return RedirectToAction("Index");
        }



        public IActionResult NoNews()
        {
            return View();
        }


        
        public IActionResult DisplayAllNews()
        {
            var allFeeds = repo.Feeds;
            var allItems = new List<Models.FeedItem>();

            foreach (var feed in allFeeds)
            {
                try
                {
                    ReadItems(feed, feed.Id, allItems);
                }
                catch (Exception)
                {
                    
                    return RedirectToAction("NoNews");
                }

                // if new items have been brought by the reader, put them in the db
                SaveNewItems(feed, allItems);
            }

            if (allFeeds.Count() != 0 && allItems != null)
            {
                // display them, newest first
                return View(new FeedViewModel
                {
                    Feeds = allFeeds,
                    FeedItems = allItems.OrderByDescending(i => i.PublishingDate)
                });
            }
            return RedirectToAction("NoNews");
        }


        public IActionResult Search()
        {
            return View(new FeedViewModel());
        }


        [HttpPost]
        public IActionResult Search(string searchString)
        {
            var allFeeds = repo.Feeds;
            var allFeedItems = repo.FeedItems;


            if (!String.IsNullOrEmpty(searchString))
            {
                return View(new FeedViewModel
                {
                    Feeds = allFeeds,
                    FeedItems = allFeedItems.Where(f => f.Title.Contains(searchString)).OrderByDescending(i => i.PublishingDate)
                });
            }

            return View(new FeedViewModel());
        }

        


        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}