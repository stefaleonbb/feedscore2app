﻿using System.Collections.Generic;

namespace MyFeeds.Models
{
    public class FeedViewModel
    {
        public Feed Feed { get; set; }
        public IEnumerable<Feed> Feeds { get; set; }
        public IEnumerable<FeedItem> FeedItems { get; set; }
    }
}
