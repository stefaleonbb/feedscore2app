﻿namespace MyFeeds.Models
{
    public class Feed
    {
        public int Id { get; set; }

        public int OwnerId { get; set; }

        public string Url { get; set; }

        public string Title { get; set; } // CodeHollow reader.Title
    }
}