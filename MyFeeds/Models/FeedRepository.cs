﻿using System.Linq;

namespace MyFeeds.Models
{
    public class FeedRepository    
    {
        private ApplicationDbContext context;

        public FeedRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Feed> Feeds => context.Feeds;
        public IQueryable<FeedItem> FeedItems => context.FeedItems;

        public void SaveFeedItem(FeedItem feedItem)
        {
            if (feedItem.Id == 0)
            {
                context.FeedItems.Add(feedItem);
            }
            context.SaveChanges();
        }

        public void SaveFeed(Feed feed)
        {
            context.Feeds.Add(feed);            
            context.SaveChanges();
        }

        public Feed DeleteFeed(int feedId)
        {
            Feed dbEntry = context.Feeds.FirstOrDefault(f => f.Id == feedId);
            if (dbEntry != null)
            {
                context.Feeds.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}
